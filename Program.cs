using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {

        static void Main(string[] args)
        {
            // The code provided will print ‘Hello World’ to the console.
            // Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.
            //Calculator.Calculator cals = new Calculator.Calculator();
            //cals.calculate();
            //string a = "absbd";
            //StringBuilder sb = new StringBuilder(a);
            //a.Remove(1,1);
            //Console.WriteLine(a);
            Calculator.Calculatorv2 calculatorv2 = new Calculator.Calculatorv2();
            //string a = "16*(8+2)";
            Console.Write("Isi soal: ");
            string a = Console.ReadLine();
            //string a = "12*(12+4)*((12/6)+4)/4";
            double result = calculatorv2.MainCalculate(a.ToCharArray());
            Console.WriteLine(result);
            Console.ReadKey();

            // Go to http://aka.ms/dotnet-get-started-console to continue learning how to build a console app! 
        }
    }
}
