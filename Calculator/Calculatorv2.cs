﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Calculator
{
    class Calculatorv2
    {
        public double MainCalculate(char[] numbers)
        {
            double a = 0;
            try
            {
                a = parsingSummand(numbers, 0);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return a;
        }

        public double parsingBracket(char[] numbers, ref int index)
        {
            char op = numbers[index];
            Console.WriteLine(op);
            if (op != '(' && op != ')')
            {
                return getDoubles(numbers, ref index);
            }
            if (op == '(')
            {
                Console.WriteLine("Awal Bracket");
                index++;
                string tampung = "";
                while (numbers[index] != ')')
                {
                    tampung += numbers[index].ToString();
                    index++;
                }
                index++;
                if (index == numbers.Length)
                {
                    index--;
                }
                double result = parsingSummand(tampung.ToCharArray(), 0);
                return result;
            }
            else
            {
                Console.WriteLine(numbers[index]);
                index++;
                Console.WriteLine(numbers[index]);
                return getDoubles(numbers, ref index);
            }
            //while (true)
            //{

            //}
        }

        private double parsingSummand(char[] numbers, int index)
        {
            Console.WriteLine("Pengurangan / Penjumlahan");
            double x = parsingFactors(numbers, ref index);
            while (true)
            {
                char op = numbers[index];
                if (op != '+' && op != '-')
                    return x;
                index++;
                double y = parsingFactors(numbers, ref index);
                if (op == '+')
                    x += y;
                else
                    x -= y;
            }
        }

        public double parsingFactors(char[] numbers, ref int index)
        {
            Console.WriteLine("Perkalian / Pembagian");
            double x = parsingBracket(numbers, ref index);
            //double x  = getDoubles(numbers, ref index);
            
            while (true)
            {
                char op = numbers[index];
                if (op != '/' && op != '*')
                    return x;
                index++;

                double y = parsingBracket(numbers, ref index);
                //double y = getDoubles(numbers, ref index);
                if (op == '/')
                    x /= y;
                else
                    x *= y;
            }
        }

        public double getDoubles(char[] numbers, ref int index)
        {
            Console.WriteLine("get doubles");
            string dbl = "";
            while (((int)numbers[index] >= 48 && (int)numbers[index] <= 57) || numbers[index] == 46)
            {
                dbl = dbl + numbers[index].ToString();
                index++;
                if (index == numbers.Length)
                {
                    index--;
                    break;
                }

            }
            return double.Parse(dbl);
        }
    }
}
